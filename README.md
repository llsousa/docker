# README

This image can be used to build packages using Docker in Docker.

Based on docker image `docker:20-10-dind`, this images adds `git` and `make` packages.

@llsousa
