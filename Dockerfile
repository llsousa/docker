FROM docker:20.10-dind

# Install packages needed to build projects: git and make
RUN set -eux; \
    apk add --no-cache \
          git \
          make